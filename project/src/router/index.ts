import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/categories',
    name: 'categories',
    component: () => import('../views/Categories.vue')//dynamický zápis, tedy při načtení stránky se nenačtou úplně všechny stránky, i když bych je třeba nepotřeboval
  },
  {
    path: '/catogory/:category_id',
    name: 'category',
    component: () => import('../views/Category.vue')//dynamický zápis, tedy při načtení stránky se nenačtou úplně všechny stránky, i když bych je třeba nepotřeboval
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '*',
    name: 'error404',
    component: () => import('../views/Error404.vue')
  }
]

const router = new VueRouter({
  mode: 'history',//je potřeba zapnout, když chci, aby uživatel mohl přecházet přes normální seo URL, je potřeba nastavit rewrite mode, jako v php, aby vše směrovalo na index.html
  base: process.env.BASE_URL,
  routes
})

export default router
